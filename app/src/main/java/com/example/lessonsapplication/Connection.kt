package com.example.lessonsapplication

import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Connection {
    val retrofit = Retrofit.Builder()
        .baseUrl("http://95.31.130.149:8083/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val api = retrofit.create(API::class.java)
}

fun <T> Call<ModelAnswer<T>>.push(
    onGetData: OnGetData<T>,
    context: Context
){
    enqueue(object:
        Callback<ModelAnswer<T>> {
        override fun onResponse(
            call: Call<ModelAnswer<T>>,
            response: Response<ModelAnswer<T>>
        ) {
            if (response.body() != null){
                if (response.body()!!.codeResponse.code == 200) {
                    onGetData.onGet(response.body()!!.body)
                }else{
                    Toast.makeText(context, response.body()!!.codeResponse.description, Toast.LENGTH_LONG).show()
                    onGetData.onError(response.body()!!.codeResponse.description)
                }
            }else{
                Toast.makeText(context, "Error ${response.code()}", Toast.LENGTH_LONG).show()
                onGetData.onError("Error ${response.code()}")
            }
        }

        override fun onFailure(call: Call<ModelAnswer<T>>, t: Throwable) {
            Toast.makeText(context, t.message ?: "Unknown error", Toast.LENGTH_LONG).show()
            onGetData.onError(t.message ?: "Unknown error")
        }
    })
}

interface OnGetData <T>{
    fun onGet(data: T)
    fun onError(error: String)
}