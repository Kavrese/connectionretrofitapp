package com.example.lessonsapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.lessonsapplication.Connection.api
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        api.signUp(ModelBodyAuth("email@gmail.com", "12345678")).push(object : OnGetData<ModelToken>{
            override fun onGet(data: ModelToken) {
                AlertDialog.Builder(this@MainActivity)
                    .setTitle("Результат")
                    .setMessage(data.token)
                    .show()
            }

            override fun onError(error: String) {

            }

        }, this)

        to_auth.setOnClickListener{
            startActivity(Intent(this, SignInActivity::class.java))
        }
    }
}