package com.example.lessonsapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.lessonsapplication.Connection.api

class CatalogActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalog)

        api.getCatalog().push(object: OnGetData<List<ModelGenreFilms>>{
            override fun onGet(data: List<ModelGenreFilms>) {
                println()
            }

            override fun onError(error: String) {

            }

        }, this)
    }
}