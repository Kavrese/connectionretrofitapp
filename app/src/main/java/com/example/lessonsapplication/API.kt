package com.example.lessonsapplication

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface API {
    @POST("signUp")
    fun signUp(@Body body: ModelBodyAuth): Call<ModelAnswer<ModelToken>>

    @POST("signIn")
    fun signIn(@Body body: ModelBodyAuth): Call<ModelAnswer<ModelToken>>

    @GET("catalog")
    fun getCatalog(): Call<ModelAnswer<List<ModelGenreFilms>>>
}