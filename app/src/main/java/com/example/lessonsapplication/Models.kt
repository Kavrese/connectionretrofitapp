package com.example.lessonsapplication

data class ModelBodyAuth(
    val email: String,
    val password: String
)

data class ModelAnswer <T>(
    val codeResponse: ModelCodeResponse,
    val body: T
)

data class ModelCodeResponse(
    val code: Int,
    val description: String
)

data class ModelToken(
    val token: String
)

data class ModelGenreFilms(
    val genre: ModelGenre,
    val films: List<ModelFilm>
)

data class ModelGenre(
    val id: Int,
    val name: String
)

data class ModelFilm(
    val id: Int,
    val title: String,
    val description: String,
    val cover: String
)